# generator-gnome-shell-extension [![Build Status](https://secure.travis-ci.org/nkx/generator-gnome-shell-extension.png?branch=master)](https://travis-ci.org/nkx/generator-gnome-shell-extension)

> [Yeoman](http://yeoman.io) generator

> [Gnome-Shell-Extension](https://wiki.gnome.org/Projects/GnomeShell/Extensions/StepByStepTutorial) tutorial

# Gnome Extension Generator #
generates an simple gnome-shell extension with a view grunt-tasks that are usefull for developing

To install generator-gnome-shell-extension from npm, run:

```bash
npm install -g generator-gnome-shell-extension
```
Finally, initiate the generator:

```bash
yo gnome-shell-extension
```

### What is this repository for? ###

* Yeoman Generator for gnome-shell extensions

### How do I get set up? ###

##### quick setup
```bash
# download the source unzip/remove zip and finaly link npm-module
wget https://bitbucket.org/nkx/generator-gnome-shell-extension/get/master.zip -O generator-gnome-shell-extension-master.zip
unzip generator-gnome-shell-extension-master.zip && rm generator-gnome-shell-extension-master.zip && cd nkx-generator-gnome-shell-* && npm link && cd ../;
# make a test
mkdir my-test-ext && cd my-test-ext && yo gnome-shell-extension
```

##### run generator
```
#!bash
# make and/or go into projectdir 
mkdir my-test-extension
cd my-test-extension
yo gnome-shell-extension
```

### Tasks ###
Run *'grunt help'* for all options

##### Extension Specific tasks

* **install** _install extension_
* **uninstall** _uninstall extension_
* **info** _show extension information_
* **reload** _reload gnome-shell_
* **link** _link extension_


### Who do I talk to? ###
* [nkx](https://bitbucket.org/nkx/)

### Contribution guidelines ###
* Writing tests
* Code review
* Other guidelines

### Anything else? ###
* [nkx](https://bitbucket.org/nkx/)
* [Learn Markdown](https://bitbucket.org/tutorials/markdowndemo)

## License
MIT