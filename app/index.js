'use strict';
var yeoman = require('yeoman-generator'),
    //beautify = require('gulp-beautify'),
    chalk = require('chalk'),
    yosay = require('yosay'),
    path = require('path'),
    fs = require('fs'),
    _ = require('lodash');

module.exports = yeoman.generators.Base.extend({
  extraTemplateDelimiter: {
    escape: /<\$-([\s\S]+?)\$>/g,
    evaluate: /<\$([\s\S]+?)\$>/g,
    interpolate: /<\$=([\s\S]+?)\$>/g
  },
  appSettings: {
    extensionName: path.basename(process.cwd()),
    vendorName: process.env.USER,
    extensionIncludes: '',
    extensionDescription: '',
    gruntEnabled: true,
    __advancedSetup: false,
  },
  initializing: function () {
    this.pkg = require('../package.json');
  },
  prompting: {
    settings: function () {
      var done = this.async();
      // Have Yeoman greet the user.
      this.log(yosay(
        'Welcome to the smashing ' + chalk.red('GnomeShellExtension') + ' generator!'
      ));
      var prompts = [{
        type: 'input',
        name: 'extensionName',
        message: 'Name of Extension?',
        default: this.appSettings.extensionName
      },{
        type: 'input',
        name: 'vendorName',
        message: 'Vendor Name?',
        default: this.appSettings.vendorName
      },{
        type: 'confirm',
        name: '__advancedSetup',
        message: 'Advanced Setup?',
        default: this.appSettings.__advancedSetup
      }];
      this.prompt(prompts, function (props) {
        this.appSettings = _.extend(this.appSettings, props);
        done();
      }.bind(this));
    },
    _additionalSettings: function(){
      if(!this.appSettings.__advancedSetup) {
        return;
      }
      var done = this.async();
      this.log('Additional Settings');

      var prompts = [{
        type: 'confirm',
        name: 'gruntEnabled',
        message: 'create grunt',
        default: this.appSettings.gruntEnabled
      }];

      this.prompt(prompts, function (props) {
        console.log(arguments);
        if(props.__advancedSetup) {
          delete props.__advancedSetup;
        }
        Object.keys(props).forEach(function (key) {
          if(!(/^__/).test(key)){
            this.appSettings[key] = props[key];
          }
        },this);
        done();
      }.bind(this));

    }
  },
  configuring: {
    setup: function(){
      // create src directory
      fs.mkdir('src', function(err){
        if(err && err.code!== 'EEXIST') {
          console.log('Error', err);
        }
      });

      // classname must be upper/camel case
      this.appSettings.extensionClassName = _.capitalize(_.camelCase(this.appSettings.extensionName));

    }
  },
  writing: {
    app: function () {
      this.fs.copyTpl(
        this.templatePath('_package.json'),
        this.destinationPath('package.json'),
        this.appSettings,
        this.extraTemplateDelimiter
      );
      this.fs.copyTpl(
        this.templatePath('_bower.json'),
        this.destinationPath('bower.json'),
        this.appSettings,
        this.extraTemplateDelimiter
      );
      if(this.appSettings.gruntEnabled) {
        this.fs.copy(
          this.templatePath('Gruntfile.js'),
          this.destinationPath('Gruntfile.js')
        );
      }
    },
    projectfiles: function () {
      this.fs.copyTpl(
        this.templatePath('src/_extension.js.tpl'),
        this.destinationPath('src/extension.js'),
        this.appSettings,
        this.extraTemplateDelimiter
      );
      this.fs.copyTpl(
        this.templatePath('src/_metadata.json'),
        this.destinationPath('src/metadata.json'),
        this.appSettings,
        this.extraTemplateDelimiter
      );
      this.fs.copyTpl(
        this.templatePath('src/stylesheet.css'),
        this.destinationPath('src/stylesheet.css'),
        this.appSettings,
        this.extraTemplateDelimiter
      );

    }
  },
  install: function () {
    this.installDependencies({
      skipInstall: this.options['skip-install']
    });
  }
});
