/*jshint camelcase: false, esnext: true, evil: true, strict: false, -W098*/
/* globals imports, log, init */
const EXTENSION_NAME = '<$= extensionName $>';
const EXTENSION_ID = '<$= extensionName $>@<$= vendorName $>';
// HEADER
const Lang = imports.lang;
const St = imports.gi.St;
const Main = imports.ui.main;
const Tweener = imports.ui.tweener;
// EXTENSION
const ExtensionUtils = imports.misc.extensionUtils;
const Extension = ExtensionUtils.extensions[EXTENSION_ID];

let button, text;
function _log(msg,opt) {
  if (!text) {
    text = new St.Label({ style_class: 'helloworld-label', text: msg });
    Main.uiGroup.add_actor(text);
  }
  text.opacity = 255;
  let monitor = Main.layoutManager.primaryMonitor;
  text.set_position(Math.floor(monitor.width / 2 - text.width / 2),
                    Math.floor(monitor.height / 2 - text.height / 2));

  var defopt = { opacity: 0, delay: 1, time: 1,
                transition: 'easeInOutQuad'};
  for(var key in opt) { defopt[key] = opt[key]; }
  Tweener.addTween(text, defopt);
}
function _iconClickHandler() {
  _log('icon click');
}

var <$= extensionClassName $> = new Lang.Class({
  Name: '<$= extensionClassName $>',
  _init: function() {
    button = new St.Bin({
      style_class: 'panel-button',
      reactive: true,
      can_focus: true,
      x_fill: true,
      y_fill: false,
      track_hover: true });

    let icon = new St.Icon({
      icon_name: 'view-refresh-symbolic',
      style_class: 'system-status-icon'
    });
    button.set_child(icon);
    try{
      button.connect('button-press-event', _iconClickHandler);
    }catch(e){
      log('Error while reloading script.'+e.toString(),{delay:3});
    }
  },
  enable: function() {
    Main.panel._rightBox.insert_child_at_index(button, 0);
  },
  disable: function() {
    Main.panel._rightBox.remove_child(button);
  }
});

function init() {
  return new <$= extensionClassName $>();
}
