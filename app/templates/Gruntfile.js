/**
 *
 * @param {[[Type]]} grunt [[Description]]
 */
module.exports = function (grunt) {
  'use strict';
  require('load-grunt-tasks')(grunt);

  var shelljs = require('shelljs'),
      GSettings = require('node-gsettings'),
      path = require('path'),
      _ = require('lodash');

  // Project configuration
  grunt.initConfig({
    // Metadata
    pkg: grunt.file.readJSON('package.json'),
    gsh: grunt.file.readJSON('src/metadata.json'),
    env: process.env,

    /*env: {
      HOME: process.env.HOME,
      USER: process.env.USER
    },*/

    banner: '/*jshint <%= pkg.jshintoptions %>*/\n/*globals <%= pkg.jshintglobals %>*/\n'+
    '/*! <%= pkg.name %> - v<%= pkg.version %> - ' +
    '<%= grunt.template.today("yyyy-mm-dd") %>\n' +
    '<%= pkg.homepage ? "* " + pkg.homepage + "\\n" : "" %>' +
    '* Copyright (c) <%= grunt.template.today("yyyy") %> <%= pkg.author.name %>;' +
    ' Licensed <%= pkg.license %> */\n',

    // Task configuration
    jshint: {
      files: ['Gruntfile.js', 'src/**.js'],
      options: {
        globals: {
          global:true, imports:true, log:true
        },
        esnext: true, evil: true
      }
    },
    clean: {
      dist: {
        src: ['dist','build'],
      },
    },
    concat: {
      options: {
        banner: '<%= banner %>',
        stripBanners: true
      },
      dist: {
        src: ['src/extension.js'],
        dest: 'dist/extension.js'
      }
    },
    shell: {
      options: {
        stderr: false
      },
      renameBuildedSrc: {
        command: 'mv ./build/src/* ./<%= gsh.uuid %>',
      },
      buildInfo: {
        command: 'ls build/** -d',
      },
      // rename build directory to dist
      makeDist: {
        command: 'mv build dist && echo "distribution done.\nall needed files in $(pwd)/dist/"',
      }
    },
    watch: {

    },
    copy: {
      main: {
        files:[{
          // Copy conconated files
          expand: true,
          flatten: false,
          src: ['src/**/*.js','src/*.{json,css}'],
          filter: 'isFile',
          dest: 'build/',
          rename: function(dest, src) {
            return dest + src.replace(/^src\//,'');
          }
        }]
      },
    }
  });



  // Default task
  grunt.registerTask('default', ['clean','jshint', 'concat']);
  grunt.registerTask('build', ['clean', 'copy:main','shell:buildInfo']);
  grunt.registerTask('dist', ['build', 'shell:makeDist']);

  var gshell = {
    // gnome settings
    gsettings: new GSettings('org.gnome.shell'),
    // extension config (from packages.json)
    config: grunt.config('pkg.config.gnome-shell-extension'),
    /**
     * enable extension (gsettings)
     * @param {String} [uuid=this.config.uuid] extension uuid
     */
    enableExtension: function(uuid) {
      uuid = uuid || this.config.uuid;
      var enabled = this.gsettings.get('enabled-extensions');
      if(enabled){
        enabled = _.unique(enabled.concat(uuid));
        this.gsettings.set('enabled-extensions',enabled);
      }
    },
    /**
     * disable extension (gsettings)
     * @param {String} [uuid=this.config.uuid] extension uuid
     */
    disableExtension: function(uuid) {
      uuid = uuid || this.config.uuid;
      var enabled = this.gsettings.get('enabled-extensions');
      if(enabled){
        enabled = _.without(enabled,uuid);
        this.gsettings.set('enabled-extensions',enabled);
      }
    },
    /**
     * install extension
     * @param {Boolean} devMode when true link src instead of copying files
     */
    installExtension: function(devMode) {
      var targetDir = this.getInstallTarget();
      if(devMode){
        this.removeExtension();
        shelljs.ln('-s', path.resolve('src'), targetDir);
      } else {
        if(!grunt.file.isDir(targetDir)) {
          shelljs.mkdir(targetDir);
        }
        shelljs.cp('-f','dist/*',targetDir);
      }
      this.enableExtension(this.config.uuid);
    },
    /**
     * remove extension
     * @param {String} [uuid=this.config.uuid] extension uuid
     */
    removeExtension: function(uuid) {
      uuid = uuid || this.config.uuid;
      var targetDir = this.getInstallTarget();
      if(shelljs.test('-L',targetDir)) {
        // remove symbolic link
        grunt.file.delete(targetDir);
      } else if(shelljs.test('-L',targetDir)) {
        // remove directory
        //console.log('Remove Extension');
        grunt.file.delete('-r', targetDir);
      }
      this.disableExtension(uuid);
    },
    /**
     * installation path
     * @returns {string} path to installed extension
     */
    getInstallTarget: function(){
      var targetDir = this.config['install-dir'];
      return targetDir+((!/\/$/.test(targetDir) && '/') || '')+this.config.uuid;
    },
    /**
     * reload gnome-shelll
     */
    reload: function(){
      shelljs.exec('gnome-shell --replace &', function () {
        console.log('Reay');
      });
    }
  };
  grunt.registerTask('enable', 'enable extension', function(){
    console.log(arguments);
  });
  grunt.registerTask('disable', 'enable extension', function(){
  });
  grunt.registerTask('uninstall', '', function(){gshell.removeExtension();});
  grunt.registerTask('install', '', function(){gshell.installExtension(grunt.option('develop'));});
  grunt.registerTask('enable', '', function(){gshell.enableExtension();});
  grunt.registerTask('disable', '', function(){gshell.disableExtension();});
  grunt.registerTask('reload', '', function(){gshell.reload();});

  grunt.registerTask('info', '', function(){
    grunt.log.subhead('Extension Informations');
    var enabled=gshell.gsettings.get('enabled-extensions');
    grunt.log.write('\nname:', gshell.config.name);
    grunt.log.write('\nuuid:', gshell.config.uuid);
    grunt.log.subhead('\nEnabled Extension');
    grunt.log.write(grunt.log.wordlist(enabled,{
      separator:', ',
      color: 'grey',
    }));
    grunt.log.write('\n\nrun \'grunt help\' for help\n');
  });
  grunt.registerTask('help', '', function(){
    grunt.log.subhead('Help');
    console.log('\ntasks:');
    grunt.log.ok('clean','- clean up');
    grunt.log.ok('build','- build the src');
    grunt.log.ok('dist','- build and distribute the src');
    console.log('\ngnome-shell tasks:');
    grunt.log.ok('install','- install extension');
    grunt.log.ok('uninstall','- uninstall extension');
    grunt.log.ok('reload','- reload gnome-shell');
    grunt.log.ok('info','- show some information');
  });
  //grunt.registerTask('install', ['copy:main','shell:renameBuildedSrc']);
};
